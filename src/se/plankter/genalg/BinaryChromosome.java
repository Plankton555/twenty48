package se.plankter.genalg;

/**
 * A chromosome containing values of either 0 or 1.
 * 
 * @author Bjorn P Mattsson
 * 
 */
public class BinaryChromosome implements IChromosome {

	/**
	 * Generates a uniformly random chromosome of the specified length.
	 * 
	 * @param chromosomeLength
	 *            The length of the chromosome
	 * @return Randomly generated chromosome
	 */
	public static BinaryChromosome getRandomChromosome(int chromosomeLength) {
		return new BinaryChromosome(chromosomeLength);
	}

	private int[] chromosome;

	/**
	 * Creates a new chromosome with the same genome as that of the original
	 * chromosome.
	 * 
	 * @param origin
	 *            Original chromosome
	 */
	private BinaryChromosome(BinaryChromosome origin) {
		this.chromosome = new int[origin.chromosome.length];

		for (int i = 0; i < origin.chromosome.length; i++) {
			chromosome[i] = origin.chromosome[i];
		}
	}

	/**
	 * Creates a new random chromosome of the specified length.
	 * 
	 * @param chromosomeLength
	 *            The length of the chromosome
	 */
	private BinaryChromosome(int chromosomeLength) {
		this.chromosome = new int[chromosomeLength];

		for (int i = 0; i < chromosomeLength; i++) {
			chromosome[i] = (int) (Math.random() * 2);
		}
	}

	/**
	 * Performs a 1-point (random) crossover on the two chromosomes.
	 * 
	 * @return Array containing two chromosomes
	 */
	@Override
	public IChromosome[] crossover(IChromosome c1, IChromosome c2) {
		if (c1.getChromosomeLength() != c2.getChromosomeLength()) {
			throw new IllegalArgumentException("Different chromosome lengths");
		}
		if (!(c1 instanceof BinaryChromosome)
				|| !(c2 instanceof BinaryChromosome)) {
			throw new IllegalStateException(
					"BinaryChromosome cannot perform crossover on something"
							+ " that is not a BinaryChromosome");
		}
		BinaryChromosome bc1 = (BinaryChromosome) c1;
		BinaryChromosome bc2 = (BinaryChromosome) c2;
		BinaryChromosome[] output = { (BinaryChromosome) bc1.getCopy(),
				(BinaryChromosome) bc2.getCopy() };
		int chromoLength = c1.getChromosomeLength();
		int crossoverPoint = (int) (Math.random() * chromoLength);
		for (int i = crossoverPoint; i < chromoLength; i++) {
			output[0].chromosome[i] = bc2.chromosome[i];
			output[1].chromosome[i] = bc1.chromosome[i];
		}
		return output;
	}

	@Override
	public int getChromosomeLength() {
		return chromosome.length;
	}

	/**
	 * @return A new chromosome with the same gene content
	 */
	@Override
	public IChromosome getCopy() {
		return new BinaryChromosome(this);
	}

	/**
	 * Returns the gene at position i.
	 * 
	 * @param i
	 *            Gene position
	 * @return Gene at position i
	 */
	public int getGene(int i) {
		return chromosome[i];
	}

	@Override
	public void mutate(double mutationProbability) {
		for (int i = 0; i < chromosome.length; i++) {
			if (Math.random() < mutationProbability) {
				// Do mutation
				if (chromosome[i] == 1) {
					chromosome[i] = 0;
				} else // if (chromosome[i] == 0)
				{
					chromosome[i] = 1;
				}
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder output = new StringBuilder("Chromosome{");
		for (int i = 0; i < chromosome.length; i++) {
			output.append(chromosome[i]);
			if (i < chromosome.length - 1) {
				output.append(",");
			}
		}
		output.append("}");

		return output.toString();
	}
	
	public int[] getNumbers()
	{
		int[] output = new int[chromosome.length];
		for (int i=0; i<chromosome.length; i++)
		{
			output[i] = chromosome[i];
		}
		return output;
	}
}
