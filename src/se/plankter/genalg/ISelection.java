package se.plankter.genalg;

/**
 * 
 * @author Bjorn P Mattsson
 *
 */
public interface ISelection {

	/**
	 * Finds and returns the index of the selected individual.
	 * 
	 * @param fitnessValues
	 *            The array of fitness values
	 * @return Index of selected individual
	 */
	int doSelection(double[] fitnessValues);
}
