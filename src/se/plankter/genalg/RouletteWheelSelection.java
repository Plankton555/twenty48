package se.plankter.genalg;

/**
 * 
 * @author Bjorn P Mattsson
 *
 */
public class RouletteWheelSelection implements ISelection {

	@Override
	public int doSelection(double[] fitnessValues) {
		int selected = -1;
		double fitnessSum = 0;
		for (int i = 0; i < fitnessValues.length; i++) {
			fitnessSum += fitnessValues[i];
		}

		double r = Math.random() * fitnessSum;

		fitnessSum = 0;
		for (int i = 0; i < fitnessValues.length; i++) {
			fitnessSum += fitnessValues[i];
			if (fitnessSum > r) {
				selected = i;
				break;
			}
		}

		if (selected < 0) {
			throw new IllegalStateException(
					"Roulette Wheel Selection failed to find a legal individual");
		}
		return selected;
	}
}
