package se.plankter.twenty48;

/**
 * The main game class.
 * 
 * @author Bj�rn PersMats
 * 
 */
public class Twenty48Game {

	/** The score goal */
	private int goal;
	/** The score gained so far */
	private int score;
	/** The highest "power-of-2" score gained so far */
	private int highestScore;

	/** The matrix containing the actual numbers */
	private int[][] matrix;
	/** The length of the matrix side */
	private int side;
	/** The Player that is performing the moves */
	private Player player;

	/** A flag for showing or suppressing console output */
	private boolean showOutput;

	/**
	 * Initializes a new game.
	 * 
	 * @param side
	 *            Number of cells per side.
	 * @param goal
	 *            The score goal. The game ends when this is reached.
	 * @param player
	 *            The Player class that handles the movement.
	 * @param showOutput
	 *            Shows console output if true, otherwise the output is
	 *            suppressed
	 */
	public Twenty48Game(int side, int goal, Player player, boolean showOutput) {
		if (side < 2 || goal < 2 || player == null) {
			throw new IllegalArgumentException("Parameters not valid");
		}
		this.side = side;
		this.goal = goal;
		this.player = player;
		this.showOutput = showOutput;

		initialize();
	}

	private void addNumber() {
		int totalSpaces = side * side;
		int occupied = getNrOccupied();

		int rnd = (int) (Math.random() * (totalSpaces - occupied));
		for (int i = 0; i < side; i++) {
			for (int j = 0; j < side; j++) {
				if (this.matrix[i][j] == 0) {
					rnd--;
					if (rnd < 0) {
						this.matrix[i][j] = 2;
						return;
					}
				}
			}
		}
	}

	/**
	 * Creates a clone/copy of a matrix that is NOT just a reference to the same
	 * matrix.
	 * 
	 * @param matrix
	 *            The matrix to copy
	 * @return The resulting matrix
	 */
	private int[][] clone2dMatrix(int[][] matrix) {
		int size = matrix.length;
		int[][] output = new int[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				output[i][j] = matrix[i][j];
			}
		}
		return output;
	}

	/**
	 * Performs a move if it is valid, otherwise does nothing.
	 * 
	 * @param input
	 *            The move that is to be performed.
	 * @return True if the move is valid, otherwise false.
	 */
	private boolean doMove(Input input) {
		int[][] oldMatrix = clone2dMatrix(this.matrix);
		int[][] tmpMatrix = clone2dMatrix(this.matrix);
		boolean valid = false;

		switch (input) {
		case DOWN:
			// rotate 0 and move
			valid = performMove(tmpMatrix);
			break;

		case LEFT:
			// rotate 270 cw and move
			rotateMatrix(tmpMatrix, 3);
			valid = performMove(tmpMatrix);
			rotateMatrix(tmpMatrix, 1);
			break;

		case RIGHT:
			// rotate 90 cw and move
			rotateMatrix(tmpMatrix, 1);
			valid = performMove(tmpMatrix);
			rotateMatrix(tmpMatrix, 3);
			break;

		case UP:
			// rotate 180 and move
			rotateMatrix(tmpMatrix, 2);
			valid = performMove(tmpMatrix);
			rotateMatrix(tmpMatrix, 2);
			break;

		default:
			break;
		}

		if (valid) {
			this.matrix = clone2dMatrix(tmpMatrix);
		} else {
			this.matrix = clone2dMatrix(oldMatrix);
		}
		return valid;
	}

	/**
	 * Draws the game if the console output is not suppressed.
	 */
	private void drawGame() {
		if (showOutput) {
			drawMatrix(this.matrix);
			System.out.println("Highest number so far: " + highestScore);
			System.out.println("Total score: " + score);
		}
	}

	/**
	 * Draws the specified matrix.
	 * 
	 * @param matrix
	 *            A 2-dimensional matrix
	 */
	private void drawMatrix(int[][] matrix) {
		int size = matrix.length;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				System.out.print(matrix[i][j] + "  ");
			}
			System.out.println();
		}
	}

	private int getNrOccupied() {
		int occupied = 0;
		for (int i = 0; i < side; i++) {
			for (int j = 0; j < side; j++) {
				if (this.matrix[i][j] != 0) {
					occupied += 1;
				}
			}
		}
		return occupied;
	}

	private void initialize() {
		this.matrix = new int[side][side];
		this.score = 0;
		this.highestScore = 0;
	}

	/**
	 * Perform one "gravity" move downwards on the specified matrix.
	 * 
	 * @param matrix
	 *            The matrix to perform the move on
	 * @return True if the move was valid, otherwise false
	 */
	private boolean performMove(int[][] matrix) {
		boolean valid = false;
		int size = matrix.length;
		for (int i = 0; i < size; i++) {
			for (int j = size - 2; j >= 0; j--) {
				if (matrix[j][i] == 0) {
					// this square does not contain a number
					continue;
				} else {
					if (matrix[j + 1][i] == 0) {
						// move square downwards
						matrix[j + 1][i] = matrix[j][i];
						matrix[j][i] = 0;

						j += 2;
						if (j > size - 1) {
							j = size - 1;
						}
						valid = true;
					} else if (matrix[j + 1][i] == matrix[j][i]) {
						// merge and generate score
						int newNr = matrix[j][i] * 2;
						matrix[j + 1][i] = newNr;
						matrix[j][i] = 0;
						highestScore = Math.max(highestScore, newNr);
						score += newNr;
						valid = true;
					} else {
						// can not move further downwards, another number in the
						// way
						continue;
					}
				}
			}
		}
		return valid;
	}

	/**
	 * Rotates an n*n-matrix 90 degrees clockwise.
	 * 
	 * @param matrix
	 *            The matrix to rotate
	 * @param nrRotations
	 *            Number of times to rotate the matrix 90 degrees clockwise. If
	 *            this is 4, the matrix will be unchanged (4*90=360 degrees).
	 */
	private void rotateMatrix(int[][] matrix, int nrRotations) {
		int n = matrix.length;
		for (int k = 0; k < nrRotations; k++) {
			int[][] tmp = clone2dMatrix(matrix);
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					matrix[i][j] = tmp[n - j - 1][i];
				}
			}
		}
	}

	/**
	 * Starts the game loop, and exits the loop when the game is either won or
	 * lost.
	 */
	public void start() {
		addNumber();
		addNumber();

		drawGame();
		while (highestScore < goal) {

			if (getNrOccupied() >= side * side && !existsValidMove()) {
				// Game over
				if (showOutput) {
					System.out.println("Game Over!");
				}
				drawGame();
				player.onEnd(clone2dMatrix(matrix), score, highestScore);
				break;
			}

			boolean validMove = doMove(player.waitForInput(
					clone2dMatrix(matrix), score, highestScore));
			int nrInvalidMoves = 0;
			int maxInvalidMoves = 50;
			while (!validMove) {
				// no valid move, do again
				nrInvalidMoves++;
				if (nrInvalidMoves > maxInvalidMoves) {
					// Break the loop and exit the game if too many invalid
					// moves have been tried
					if (showOutput) {
						System.out.println(maxInvalidMoves
								+ " invalid moves tried, exiting game...");
					}
					player.onEnd(clone2dMatrix(matrix), score, highestScore);
					return;
				}
				if (showOutput) {
					System.out.println("Invalid move, please try again");
				}
				validMove = doMove(player.waitForInput(clone2dMatrix(matrix),
						score, highestScore));
			}

			addNumber();
			drawGame();
		}
	}

	/**
	 * Checks whether there exists a valid move that can be performed.
	 * 
	 * @return True if there is a valid move, false if there is no valid move.
	 */
	private boolean existsValidMove() {
		// TODO Write this! (not extremely important, depends on the game rules
		return false;
	}
}
