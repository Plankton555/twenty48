package se.plankter.twenty48;

/**
 * An interface for players.
 * 
 * @author Bj�rn PersMats
 * 
 */
public interface Player {

	/**
	 * Waits for game input from a player class. The game state is provided via
	 * the parameters.
	 * 
	 * @param matrix
	 *            The game number matrix
	 * @param score
	 *            The current score
	 * @param highestScore
	 *            The currently highest "power-by-2" score
	 * @return The input to the game
	 */
	Input waitForInput(int[][] matrix, int score, int highestScore);

	/**
	 * Is called when the game ends.
	 * 
	 * @param matrix
	 *            The end state of the game number matrix
	 * @param score
	 *            The end score
	 * @param highestScore
	 *            The highest "power-by-2" score at the end of the game
	 */
	void onEnd(int[][] matrix, int score, int highestScore);
}
