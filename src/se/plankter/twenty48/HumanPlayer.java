package se.plankter.twenty48;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * A player class that prompts for human input in the console.
 * 
 * @author Bj�rn PersMats
 * 
 */
public class HumanPlayer implements Player {

	@Override
	public Input waitForInput(int[][] matrix, int score, int highestScore) {
		Input input = null;
		while (input == null) {
			System.out.println("Human player waiting for input (wasd): ");
			try {
				BufferedReader bufferRead = new BufferedReader(
						new InputStreamReader(System.in));
				String s = bufferRead.readLine();
				if (s.equalsIgnoreCase("w")) {
					input = Input.UP;
				} else if (s.equalsIgnoreCase("s")) {
					input = Input.DOWN;
				} else if (s.equalsIgnoreCase("a")) {
					input = Input.LEFT;
				} else if (s.equalsIgnoreCase("d")) {
					input = Input.RIGHT;
				} else {
					System.out.println("Could not read input");
					input = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return input;
	}

	@Override
	public void onEnd(int[][] matrix, int score, int highestScore) {
		// TODO Auto-generated method stub

	}

}
