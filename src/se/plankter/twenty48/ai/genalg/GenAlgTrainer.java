package se.plankter.twenty48.ai.genalg;

import se.plankter.genalg.BinaryChromosome;
import se.plankter.genalg.GeneticAlgorithm;
import se.plankter.genalg.IChromosome;
import se.plankter.genalg.IChromosomeDecoder;
import se.plankter.genalg.ISelection;
import se.plankter.genalg.TournamentSelection;
import se.plankter.twenty48.Twenty48Game;

public class GenAlgTrainer implements IChromosomeDecoder {

	private static double P_TOUR = 0.75;

	public static void main(String[] args) {

		// Each direction coded by 2 binaries
		// 00 -> UP, 01 -> RIGHT, 10 -> DOWN, 11 -> LEFT
		int chromosomeLength = 2 * 8;
		int populationSize = 20;

		BinaryChromosome[] population = new BinaryChromosome[populationSize];
		for (int i = 0; i < populationSize; i++) {
			population[i] = BinaryChromosome
					.getRandomChromosome(chromosomeLength);
		}

		ISelection selection = new TournamentSelection(P_TOUR);
		IChromosomeDecoder decoder = new GenAlgTrainer();

		GeneticAlgorithm ga = new GeneticAlgorithm(population, selection,
				decoder);

		double bestFitness = 0;
		IChromosome bestChromosome = null;
		for (int i = 0; i < 1000; i++) {
			ga.runGeneration();
			double generationFitness = ga.getBestFitness();
			System.out.println("G: " + (i + 1) + "\tfitness: "
					+ generationFitness);
			printSolution((BinaryChromosome) ga.getBestChromosome());

			if (generationFitness > bestFitness) {
				bestFitness = generationFitness;
				bestChromosome = ga.getBestChromosome();
				System.out.println("  New best fitness\n");
			}
		}

		System.out.println("\n\nGlobal best solution\nFitness: " + bestFitness);
		printSolution((BinaryChromosome) bestChromosome);
	}

	private static void printSolution(BinaryChromosome chromosome) {
		StringBuilder output = new StringBuilder();
		int[] numbers = chromosome.getNumbers();
		for (int i = 0; i < numbers.length; i += 2) {
			String dir = "";
			if (numbers[i] == 0 && numbers[i + 1] == 0) {
				dir = "UP";
			} else if (numbers[i] == 0 && numbers[i + 1] == 1) {
				dir = "RIGHT";
			} else if (numbers[i] == 1 && numbers[i + 1] == 0) {
				dir = "DOWN";
			} else if (numbers[i] == 1 && numbers[i + 1] == 1) {
				dir = "LEFT";
			}
			output.append(dir + " ");
		}
		System.out.println("Solution: " + output.toString() + "\n");
		// System.out.println("Raw: " + chromosome.toString());
	}

	@Override
	public double decodeChromosome(IChromosome chromosome) {
		if (!(chromosome instanceof BinaryChromosome)) {
			throw new IllegalStateException("Must be a BinaryChromosome");
		}
		BinaryChromosome binChromosome = (BinaryChromosome) chromosome;
		GenAlgPlayer player = new GenAlgPlayer(binChromosome);

		int nrTries = 30;
		double summedScore = 0;
		for (int i = 0; i < nrTries; i++) {
			Twenty48Game game = new Twenty48Game(4, 2048, player, false);

			game.start();

			summedScore += player.getScore();
		}

		return summedScore / nrTries;
	}

}
