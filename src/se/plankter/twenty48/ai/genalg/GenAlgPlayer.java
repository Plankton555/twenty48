package se.plankter.twenty48.ai.genalg;

import se.plankter.genalg.BinaryChromosome;
import se.plankter.twenty48.Input;
import se.plankter.twenty48.Player;

public class GenAlgPlayer implements Player {

	private double score;

	private int counter;
	private BinaryChromosome sequence;

	public GenAlgPlayer(BinaryChromosome chromosome) {
		this.counter = 0;
		this.sequence = chromosome;
	}

	@Override
	public Input waitForInput(int[][] matrix, int score, int highestScore) {
		// this.score = score;

		int[] numbers = sequence.getNumbers();
		Input dir = Input.DOWN;
		if (numbers[counter] == 0 && numbers[counter + 1] == 0) {
			dir = Input.UP;
		} else if (numbers[counter] == 0 && numbers[counter + 1] == 1) {
			dir = Input.RIGHT;
		} else if (numbers[counter] == 1 && numbers[counter + 1] == 0) {
			dir = Input.DOWN;
		} else if (numbers[counter] == 1 && numbers[counter + 1] == 1) {
			dir = Input.LEFT;
		}
		counter += 2;
		counter %= numbers.length;
		return dir;
	}

	@Override
	public void onEnd(int[][] matrix, int score, int highestScore) {
		this.score = score;
	}

	public double getScore() {
		return score;
	}

}
