package se.plankter.twenty48;

public class Main2048 {

	public static void main(String[] args) {
		Player player = new HumanPlayer();
		Twenty48Game game = new Twenty48Game(4, 2048, player, true);
		
		game.start();
	}

}
